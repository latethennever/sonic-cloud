# 版本迁移指南

本文将介绍如何升级到最新版本。

## 关于simple版本
综合注册架构、通信逻辑、性能优化等因素，从v1.4.0-beta开始，集群版与simple版合并，并且在sonic-server上继续维护，因此sonic-server-simple版本不再维护。

## 其他旧版本升级到v1.4.1-release
> 0. server升级前，先备份数据库。
> 1. **注意！** 升级后图片路径有所更改，所以旧图片会失效！
> 
> > 解决方案：
> > 1. 可以在数据库将旧图片路径的 **ip:port** 替换为 **ip:前端port/server**，即可恢复（推荐）。
> > 2. 临时将docker-compose.yml文件的gateway服务，加上ports映射（不推荐），以下为示例代码：
> ```
> sonic-server-gateway:
>     image: "registry.cn-hangzhou.aliyuncs.com/sonic-cloud/sonic-server-gateway:v1.4.1-beta"
>     hostname: sonic-server-gateway
>     environment:
>       - SONIC_EUREKA_USERNAME
>       - SONIC_EUREKA_PASSWORD
>       - SONIC_EUREKA_PORT
>       - SONIC_EUREKA_HOST=sonic-server-eureka
>       - SECRET_KEY
>       - EXPIRE_DAY
>     volumes:
>       - ./logs/:/logs/
>     depends_on:
>       - sonic-server-eureka
>     networks:
>       - sonic-network
>     restart: on-failure
>     ports:
>       - 8094:8094
> ```
> 
> 2. 因新版本 **将前后端多个端口合并为单一接口** 暴露，配置时需注意。（注意env文件与docker-compose.yml文件都需要更换）
> 3. 因整体架构调整较多，可前往 **前后端部署** 页面重新部署。
> 4. server升级后，将旧挂载目录下的logs、keepFiles、imageFiles、recordFiles、packageFiles中的内容迁移到新目录下。
> 5. Agent改动较大，需要将旧文件全部删除重新部署。配置yml时注意server的port应为 **SONIC_SERVER_PORT**。

## 其他旧版本升级到v1.4.0-release

> 1. server升级前，先备份数据库。
> 2. 因新版本调整设备数据，尽量将旧版本的 **重复序列号(udId)** 的设备删除至剩一个。（可查看下方常见问题）
> 3. 因整体架构调整较多，可前往 **前后端部署** 页面重新部署。
> 4. server升级后，将旧挂载目录下的logs、keepFiles、imageFiles、recordFiles、packageFiles中的内容迁移到新目录下。
> 5. 如果旧版本没有自定义挂载路径（特别是simple版用户），您不知道您的本地文件默认储存在哪里，可以 [参考这里](https://blog.csdn.net/wu_qing_song/article/details/113253437) （如果你不想使用这个方法，可以参考常见问题第二题）。
> 6. Agent改动较大，需要将旧文件全部删除重新部署。

## 常见问题

> 1. 我如果不迁移上文提到的若干个文件夹，直接使用新版本自带的文件夹，会发生什么？
> > 旧版本的图片、录像、截图等信息将失效。当然如果文件不多，直接用新版本也是可以的。
> 2. 如果不用上文第四点的方法，还有别的方法吗？
> > 可以使用 docker cp 指令将容器内的文件复制到宿主机，再迁移到新版本文件夹中。
> 3. 为什么旧版本需要删除重复的序列号设备？
> > 旧版本是以单Agent内不可重复，后续考虑到用户会将同一设备在多个Agent之间迁移，直接以序列号为标识。安卓目前是极低概率才会出现序列号相同的设备（哪怕相同也有办法修改），而苹果的序列号都是唯一的，所以大家不用担心。
> 
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;更多疑问可前往👉[社区](https://sonic-cloud.wiki)👈交流

## 本文贡献者
<div class="cont">
<a href="https://gitee.com/ZhouYixun" target="_blank">
<img src="https://portrait.gitee.com/uploads/avatars/user/2698/8096045_ZhouYixun_1645499109.png!avatar100" width="50"/>
<span>ZhouYixun</span>
</a>
</div>


&nbsp;
&nbsp;
***
不够详细？[点此](https://gitee.com/sonic-cloud/sonic-cloud/edit/master/src/markdown/deploy/upgrade-deploy.md) 发起贡献改善此页