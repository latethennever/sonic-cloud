# sonic-android-apk
本文为Sonic安卓助手sonic-android-apk的介绍与原理简述。 👉[Github地址](https://github.com/SonicCloudOrg/sonic-android-apk)

<a href="#">  
<img src="https://img.shields.io/github/stars/SonicCloudOrg/sonic-android-apk?style=social">
<img style="margin-left:10px" src="https://img.shields.io/github/forks/SonicCloudOrg/sonic-android-apk?style=social">
</a>

## 本仓库贡献者

<a href="https://github.com/SonicCloudOrg/sonic-android-apk/graphs/contributors">
  <img src="https://contrib.rocks/image?repo=SonicCloudOrg/sonic-android-apk" />
</a>

## 介绍

> **sonic-android-apk** 是Sonic平台使用的安卓助手（目前还需搭配Sonic平台一并使用），主要利用安卓SDK暴露的API扩展了adb无法做到的交互，用于打造更流畅更精品的安卓远控体验，如：
> 1. 远程音频AAC传输
> 2. App图标实时获取
> 3. 实时触控
> 4. 等等...
> 
> 后续会继续扩展更多新鲜好玩的功能，也欢迎大家一起参与建设~

## 快速使用

1. **一般Agent端已自带** ，您也可以选择单独下载。（如链接失效，请自行前往 <a href="https://github.com/SonicCloudOrg/sonic-android-apk/releases" target="_blank">这里</a> 下载）
>
>  > 👉<a href="https://download.sonic-cloud.wiki/sonic/sonic-android-apk/v2.0.1/sonic-android-apk.apk" target="_blank">sonic-android-apk.apk</a>
>  


## 本文贡献者
<div class="cont">
<a href="https://gitee.com/ZhouYixun" target="_blank">
<img src="https://portrait.gitee.com/uploads/avatars/user/2698/8096045_ZhouYixun_1645499109.png!avatar100" width="50"/>
<span>ZhouYixun</span>
</a>
</div>


&nbsp;
&nbsp;
***
不够详细？[点此](https://gitee.com/sonic-cloud/sonic-cloud/edit/master/src/markdown/saa/re-saa.md) 发起贡献改善此页

