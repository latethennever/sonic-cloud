# sonic-go-mitmproxy
本文为Sonic网络抓包代理sonic-go-mitmproxy的介绍与原理简述。 👉[Github地址](https://github.com/SonicCloudOrg/sonic-go-mitmproxy)

<a href="#">  
<img src="https://img.shields.io/github/stars/SonicCloudOrg/sonic-go-mitmproxy?style=social">
<img style="margin-left:10px" src="https://img.shields.io/github/forks/SonicCloudOrg/sonic-go-mitmproxy?style=social">
</a>

## 本仓库贡献者

<a href="https://github.com/SonicCloudOrg/sonic-go-mitmproxy/graphs/contributors">
  <img src="https://contrib.rocks/image?repo=SonicCloudOrg/sonic-go-mitmproxy" />
</a>

## 介绍

> **sonic-go-mitmproxy** 是基于 [go-mitmproxy](https://github.com/lqqyt2423/go-mitmproxy) 建设的抓包代理助手。
> > Sonic组织也在持续将代理抓包的探索 **共建** 到go-mitmproxy上，以下是Sonic组织目前参与建设的提交：
> >1. 新增可自定义证书生成路径与cer文件生成。[feat: 增加自定义证书生成路径、增加cer文件生成](https://github.com/lqqyt2423/go-mitmproxy/commit/8522ebedb0cbd52fae1bef2423697a9eba5ca161)
> >2. 调整抓包页面顶部置顶样式调整。[feat: 顶部样式置顶调整](https://github.com/lqqyt2423/go-mitmproxy/commit/29cf59bbbf1f21ff0c524743f32b693f632f7af5)
> >3. 调整抓包页面表格自适应宽度。[feat: 更改表宽度自适应](https://github.com/lqqyt2423/go-mitmproxy/commit/6d5ce792ce8e76db92f915eb6969696be1f8224e)
>
> 后续会继续扩展更多新鲜好玩的功能，也欢迎大家一起参与建设~

## 快速使用
建设中...

## 本文贡献者
<div class="cont">
<a href="https://gitee.com/ZhouYixun" target="_blank">
<img src="https://portrait.gitee.com/uploads/avatars/user/2698/8096045_ZhouYixun_1645499109.png!avatar100" width="50"/>
<span>ZhouYixun</span>
</a>
</div>


&nbsp;
&nbsp;
***
不够详细？[点此](https://gitee.com/sonic-cloud/sonic-cloud/edit/master/src/markdown/sgm/re-sgm.md) 发起贡献改善此页