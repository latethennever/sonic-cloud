# sonic-ci-helper
本文为Sonic Jenkins插件sonic-ci-helper的介绍与原理简述。 👉[Github地址](https://github.com/jenkinsci/sonic-ci-helper-plugin)

<a href="#">  
<img src="https://img.shields.io/github/stars/jenkinsci/sonic-ci-helper-plugin?style=social">
<img style="margin-left:10px" src="https://img.shields.io/github/forks/jenkinsci/sonic-ci-helper-plugin?style=social">
</a>

## 本仓库贡献者

<a href="https://github.com/jenkinsci/sonic-ci-helper-plugin/graphs/contributors">
  <img src="https://contrib.rocks/image?repo=jenkinsci/sonic-ci-helper-plugin" />
</a>

## 介绍

> **sonic-ci-helper** 是为对接Jenkins CI流程建设的小助手。计划可以对接CI流程所需要的步骤。
>
> 后续会继续扩展更多新鲜好玩的功能，也欢迎大家一起参与建设~

## 快速使用

### 一、在线安装

> 1. 进入 【管理Jenkins】 > 【插件管理】 视图
> 2. 搜索Sonic CI Helper安装即可
> <el-image hide-on-click-modal src="https://img.wenjie.store/2022-07-27/1658886552-595597-9a1d8b2970d61eaebd9b806a13e373c.png"
> :preview-src-list="['https://img.wenjie.store/2022-07-27/1658886552-595597-9a1d8b2970d61eaebd9b806a13e373c.png']" style="width: 80%"/>

### 二、离线安装

> 1. 点击下方链接下载到任意目录。（如链接失效，请点击 <a href="https://repo.jenkins-ci.org/artifactory/releases/io/jenkins/plugins/sonic-ci-helper/1.0.2/sonic-ci-helper-1.0.2.hpi" target="_blank">这里</a> 下载）
>
>  > 👉<a href="https://download.sonic-cloud.wiki/sonic/sonic-ci-helper/v1.0.2/sonic-ci-helper-1.0.2.hpi" target="_blank">sonic-ci-helper-1.0.2.hpi</a>
> 2. 前往Jenkins的插件管理页面，选择手动安装插件，将hpi上传即可。

## 本文贡献者
<div class="cont">
<a href="https://gitee.com/ZhouYixun" target="_blank">
<img src="https://portrait.gitee.com/uploads/avatars/user/2698/8096045_ZhouYixun_1645499109.png!avatar100" width="50"/>
<span>ZhouYixun</span>
</a>
</div>


&nbsp;
&nbsp;
***
不够详细？[点此](https://gitee.com/sonic-cloud/sonic-cloud/edit/master/src/markdown/sgm/re-sch.md) 发起贡献改善此页